var config = require('./config.js');
var express = require('express');
var nunjucks = require('express-nunjucks');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var sassMiddleware = require('node-sass-middleware');

var routes = require('./routes/index');

var app = express();

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use('/public', express.static(path.join(__dirname, 'public')));
app.use('/images', express.static(path.join(__dirname, 'public/images')));
app.use('/javascript', express.static(path.join(__dirname, 'public/javascripts')));
app.use('/documents', express.static(path.join(__dirname, 'public/documents')));

// Use sass middleware
app.use('/styles', sassMiddleware({
  src: __dirname + '/sass',
  dest: __dirname + '/public/stylesheets/',
  debug: false,
  outputStyle: 'expanded'
}));

// Add variables that are available in all views
app.use(function (req, res, next) {
  res.locals.template = config.template;
  res.locals.serviceName = config.serviceName;
  next();
});

app.use('/styles', express.static(path.join(__dirname, 'public/stylesheets/')));


// Setup nunjucks templating engine
app.set('view engine', 'html');
app.set('views', __dirname + '/views');
nunjucks.setup({
    autoescape: true,
    watch: true,
    noCache: true
}, app);

// routes (found in app/routes.js)
if (typeof(routes) != "function"){
    console.log(routes.bind);
    console.log("Warning: the use of bind in routes is deprecated - please check the prototype kit documentation for writing routes.")
    routes.bind(app);
} else {
    app.use("/", routes);
}

// Strip .html and .htm if provided
app.get(/\.html?$/i, function (req, res){
    var path = req.path;
    var parts = path.split('.');
    parts.pop();
    path = parts.join('.');
    res.redirect(path);
});

// auto render any view that exists
app.get(/^\/([^.]+)$/, function (req, res) {

    var path = (req.params[0]);

    res.render(path, function(err, html) {
        if (err) {
            res.render(path + "/index", function(err2, html) {
                if (err2) {
                    console.log(err);
                    res.status(404).send(err + "<br>" + err2);
                } else {
                    res.end(html);
                }
            });
        } else {
            res.end(html);
        }
    });

});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error.html', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error.html', {
    message: err.message,
    error: {}
  });
});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});


module.exports = app;
